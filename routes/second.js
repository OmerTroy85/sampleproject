/**
 * Created by Omer on 7/25/2017.
 */
var express = require('express');
var router = express.Router();

router.get('/customers',function (req,res) {
    var data = req.app.get('appData');
    var data1 = data.customerData;
    var dataNames = [];
    var dataAvatar = [];
    var dataCompany = [];
    data1.forEach(function (item) {
        dataNames = dataNames.concat(item.name);
        dataAvatar = dataAvatar.concat(item.avatar);
        dataCompany = dataCompany.concat(item.company);
    });

    res.render('customers',{
        pageTitle: 'Our Customers',
        pageID: 'customers',
        entries: data1,
        customerNames: dataNames,
        customerAvatars: dataAvatar,
        customerCompanies: dataCompany
    });
});

module.exports = router;