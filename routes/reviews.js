/**
 * Created by Omer on 7/25/2017.
 */
var express = require('express');
var router = express.Router();

router.get('/reviews',function (req,res) {
    var data = req.app.get('appData');
    var data1 = data.customerData;

    res.render('reviews', {
        pageTitle: 'Reviews',
        pageID: 'reviews',
        entries: data1
    });
});

module.exports = router;