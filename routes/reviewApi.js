var express = require('express');
var router  = express.Router();
var bodyparser = require('body-parser');
var apidata = require('../Data/reviews.json');
var fs = require('fs');

router.get('/reviewapi',function(req,res){
    res.json(apidata);
});

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));

router.post('/reviewapi',function(req,res){
    apidata.unshift(req.body);
    fs.writeFile('Data/reviews.json', JSON.stringify(apidata), 'utf8', function(err) {
        console.log(err);
    });
    res.json(apidata);
});
module.exports = router;