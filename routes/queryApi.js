var express = require('express');
var router  = express.Router();
var bodyparser = require('body-parser');
var queryData = require('../Data/query.json');
var fs = require('fs');

router.get('/queryapi',function(req,res){
    res.json(queryData);
});

router.use(bodyparser.json());
router.use(bodyparser.urlencoded({extended: false}));

router.post('/queryapi',function(req,res){
    queryData.unshift(req.body);
    fs.writeFile('Data/query.json', JSON.stringify(queryData), 'utf8', function(err) {
        console.log(err);
    });
    res.json(queryData);
});
module.exports = router;