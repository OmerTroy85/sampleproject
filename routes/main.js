/**
 * Created by Omer on 7/25/2017.
 */
var express = require('express');
var router  = express.Router();



router.get('/',function(req,res){


    res.render('index',{
        pageTitle: 'Home Page',
        pageId: 'home'
    });
});

module.exports = router;
