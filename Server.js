var express = require('express');
var app = express(); // the main app
var reload = require('reload');
var io = require('socket.io')();

app.use(express.static('view'));
app.use(require('./routes/main'));
app.use(require('./routes/second'));
app.use(require('./routes/reviews'));
app.use(require('./routes/api'));
app.use(require('./routes/reviewApi'));
app.use(require('./routes/chatroute'));
app.use(require('./routes/queryApi'));

app.set('view engine', 'ejs');
app.set('views','view');

var dataFile = require('./Data/newData.json');
app.set('appData' , dataFile);

app.set('port', process.env.PORT || 8000);

var server = app.listen(app.get('port'), function(){
    console.log("server started");
    console.log("Access the layout by typing localhost:8000/");
});

io.attach(server);

io.on('connection', function(socket){
    console.log('User Connected');

    socket.on('disconnect', function(){
        console.log('User Disconnected');
    })
});
reload(server, app );
// https://jsonplaceholder.typicode.com/users
